# Complete NDK3.9

**Description**: Missing pieces from the Amiga OS NDK3.9 for the assembler programmer 

* **LVO**: When NDK3.9 was released, the Library Vector offsets were not included. These had to be generated, by a tool, or manually.
* **Missing includes**: There were missing and erroneous includes.

**Project Status**:  https://gitlab.com/amigasourcecodepreservation/complete-ndk39/blob/master/CHANGELOG.adoc[CHANGELOG]

## Dependencies

  * Amiga OS Classic
  * http://www.haage-partner.de/download/AmigaOS/NDK39.lha[The Amiga OS 3.9 NDK]

## Installation

  * Put the LVOs.i file in your NDK3.9 include-folder.
  * Copy the content of the include-updates folder to your NDK3.9 .include directory

## Getting help

If you have questions, concerns, bug reports, etc, please file an issue in this repository's Issue Tracker.

## Getting involved

Contact your old amiga friends and tell them about our project, and ask them to dig out their source code or floppies and send them to us for preservation.

Clean up our hosted archives, and make the source code buildable with standard compilers like devpac, asmone, gcc 2.9x/Beppo 6.x, sas/c ,vbcc and friends.


Cheers!

....

     _____ ___   _   __  __     _   __  __ ___ ___   _   
    |_   _| __| /_\ |  \/  |   /_\ |  \/  |_ _/ __| /_\  
      | | | _| / _ \| |\/| |  / _ \| |\/| || | (_ |/ _ \ 
     _|_| |___/_/ \_\_|_ |_|_/_/_\_\_|__|_|___\___/_/_\_\
    / __|/ _ \| | | | _ \/ __| __|  / __/ _ \|   \| __|  
    \__ \ (_) | |_| |   / (__| _|  | (_| (_) | |) | _|   
    |___/\___/_\___/|_|_\\___|___|__\___\___/|___/|___|_ 
    | _ \ _ \ __/ __| __| _ \ \ / /_\_   _|_ _/ _ \| \| |
    |  _/   / _|\__ \ _||   /\ V / _ \| |  | | (_) | .` |
    |_| |_|_\___|___/___|_|_\ \_/_/ \_\_| |___\___/|_|\_|

....

https://twitter.com/AmigaSourcePres[Twitter]
https://gitlab.com/amigasourcecodepreservation[Gitlab] 
https://amigasourcepres.gitlab.io/[WWW]

## Licensing

Complete-ndk3.9 is distributed under the terms of the Create Commons CC0 1.0 Universal License. See the https://gitlab.com/amigasourcecodepreservation/complete-ndk39/blob/master/LICENSE[LICENSE] file for details.

In other words - it is "public domain", but we appreciate if you send us any updates.

## Credits and references

Many thanks to Harry Sintonen (original http://aminet.net/package/dev/asm/incupd[include updates] and PeterK (put together the original LVO-list).

